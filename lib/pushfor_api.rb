class PushforApi
  require 'faraday'
  require 'json'
  require 'faraday-cookie_jar'
  require 'faraday_middleware'
  require './features/support/env'
  require 'base64'
  require 'pushfor_websocket'

  def initialize(user, password, login = true)
        url = if ENV['ENV'] == 'test' then EnvConfig.get :application_test_url
              elsif ENV['ENV'] == 'dev'then EnvConfig.get :application_dev_url
              end
        @connection = Faraday.new(url) do |faraday|
          faraday.use :cookie_jar
          faraday.request :url_encoded
          faraday.request :multipart
          # faraday.response :logger
          faraday.response :json, content_type: 'application/json'
          faraday.adapter :net_http
          faraday.options.timeout = 120
        end

        if login
          login(user,password)
          # puts "Login via API as #{user} with #{password} pass"
        else
          # puts "Initialize PushforAPI without login"
        end
  end

  def send_post(url, body, type='status')
    response = @connection.post(url, body)
    case type
      when 'status'
        response.status
      when 'body'
        response.body
    end
  end

  def send_post_request(url, body, type='status')
    response = @connection.post do |req|
      req.url url
      req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      req.headers['Accept'] = 'application/json'
      req.body = body
    end
    case type
      when 'status'
        response.status
      when 'body'
        response.body
    end
  end

  def send_post_file(url, body, file, status)
    response = @connection.post do |req|
      req.url url
      req.headers['Content-Disposition'] = "form-data; name='file'; filename='#{file}'"
      req.headers['Content-Type'] = 'multipart/form-data'
      req.headers['Accept'] = "application/json"
      req.body = body
    end
    case status
      when 'status'
        return response.status
      when 'body'
        return response.body
    end
  end

  def send_post_with_file(url, attach, body)
    response = @connection.post do |req|
      req.url url
      # req.headers['Content-Disposition'] = "form-data; name='file'; filename='#{attach}'"
      req.headers['Content-Type'] = "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
      req.headers['Accept'] = "application/json"
      req.body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"users\"\r\n\r\n[\"#{body}\"]\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"file\"; filename=\"#{attach}\"\r\nContent-Type: image/png\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
    end
    response.body
  end

  def send_get(url)
    response = @connection.get url
    response.status
  end

  def login(username, password)
    user = {
        username: username,
        password: password,
        version: 2
    }
    response = send_post('/api/login', user)
    return response
  end

  def send_register_confirmation(email)
    email = { email: email}
    response = send_post('/api/register/send_confirmation', email)
  return response
  end

  def register(username, email, password, status)
    host =  if ENV['ENV'] == 'test'then EnvConfig.get :host_test_url
    elsif ENV['ENV'] == 'dev'then EnvConfig.get :host_dev_url
    end
    user = {
        username: username,
        email: email,
        password: password,
        version: 2,
        host: host
    }
    response = send_post('/api/register',user,status)
    return response
  end

  def confirm_reg(hash)
    hash = {hash: hash}
    response = send_post('/api/confirm/reg', hash, 'body')
  end

  def login_via_facebook
    user = {
        id: (EnvConfig.get :facebook_id),
        name: 'Pushfor Qa',
        type: 'facebook',
        token: (EnvConfig.get :facebook_token),
        email: 'pushforqa@gmail.com',
        version: 2
    }
    response = send_post('/api/login/oauth2', user)
    return response
  end

  def login_via_google
    id = EnvConfig.get :google_id
    name = 'pushfor test'
    type = 'google'
    token = EnvConfig.get :google_token
    email = 'pushforqa@gmail.com'
    version = 2

    user =  "email=#{ERB::Util.url_encode(email)}&"\
            "id=#{ERB::Util.url_encode(id)}&"\
            "name=#{ERB::Util.url_encode(name)}&"\
            "remember_me=0&"\
            "token=#{ERB::Util.url_encode(token)}&"\
            "type=#{ERB::Util.url_encode(type)}&"\
            "version=#{ERB::Util.url_encode(version)}"\

    send_post('/api/login/oauth2', user)
  end

  def login_via_linkedin
    id = EnvConfig.get(:link_id)
    name = 'Pushfor Qa'
    type = 'linkedin'
    token = EnvConfig.get(:link_id)
    email = 'pushforqa@gmail.com'
    version = '2'
    access_token = EnvConfig.get(:link_access_token)
    signature = EnvConfig.get(:link_signature)
    cookie = "{\"member_id\":\"xVxoWod_bn\",\"access_token\":\"#{access_token}\",\"signature_order\":[\"access_token\",\"member_id\"],\"signature\":\"#{signature}\",\"signature_version\":\"1\",\"signature_method\":\"HMAC-SHA1\"}"

    user =  "cookie=#{ERB::Util.url_encode(cookie)}&"\
            "email=#{ERB::Util.url_encode(email)}&"\
            "id=#{ERB::Util.url_encode(id)}&"\
            "name=#{ERB::Util.url_encode(name)}&"\
            "remember_me=0&"\
            "token=#{ERB::Util.url_encode(token)}&"\
            "type=#{ERB::Util.url_encode(type)}&"\
            "version=#{ERB::Util.url_encode(version)}"\

    send_post('/api/login/oauth2', user)
  end

  def logout()
    send_post('/api/logout','')
  end

  def change_password_to_new(new_password, link_hash)
    body = {
        password: new_password,
        forgot_password: link_hash
    }
    send_post('/api/change_forgotten_password', body)
  end

  def send_reset_password_link(email)
    body = {
        email: email
    }
    send_post('/api/forgot_password', body)
  end

  def change_user_info(username,email,password,color=nil,phone=nil,file=nil)
    body = {
        usename: username,
        color: color,
        email: email,
        password: password,
        phone: phone,
        file: file
    }
    send_post('/api/user/info/change', body, 'body')
  end

  def get_user_info
    send_post('/api/user/info', '', 'body')
  end

  def upload_file(status)
    attachment = "attachments/1.png"
    response = send_post('/api/library/get_root', '', 'body')
    body = {
        file: Faraday::UploadIO.new(attachment.to_s, 'image/png'),
        folder_id: response['id']
    }
    send_post_file('/api/library/add_file', body, attachment, status)
  end

  def search_file(name, folder_id, type)
    body = {
        folder_id: folder_id,
        name: name
    }
    send_post('/api/library/search', body, type)
  end

  def delete_file(file_id, type)
    body = {
        file_id: file_id
    }
    send_post('/api/library/delete_file', body, type)
  end

  def delete_all_files()
    response = send_post('/api/library/get_root', '', 'body')
    files = Array.new
    response['children'].each do |hash|
      files << hash['id']
    end

    test = ERB::Util.url_encode("{\"folders\":[],\"files\":#{files}}")
    body = "ids=#{test}"

    send_post_request('/api/library/delete', body, 'body')
  end

  def create_folder(name = 'test')
    response = send_post('/api/library/get_root', '', 'body')
    parent_id = response['id']
    body = {
        parent_id: parent_id,
        name: name
    }
    send_post('/api/library/create_folder', body, 'status')
  end

  def get_file_info(id)
    body = {
        file_id: id
    }
    send_post('/api/library/get_file', body, 'body')
  end

  def download_file(hash)
    body = {
        hash: hash
    }
    link = send_post('/api/download/get_link', body, 'body')
    send_get(link['link'].gsub('http://','https://'))
  end

  def create_chat(file_id, user, group, comment='comment')

    if user.empty?
      groups = "#{ERB::Util.url_encode("[\"#{group}\"]")}"
      body="file_id=#{file_id}&groups=#{groups}&comment=#{comment}"
    else
      users = "#{ERB::Util.url_encode("[\"#{user}\"]")}"
      body="file_id=#{file_id}&users=#{users}&comment=#{comment}"
    end

    send_post('/api/create_chat', body, 'body')
  end

  def create_chat_with_attach(user)
    attachment = "attachments/1.png"
    # body = {
    #     file: Faraday::UploadIO.new(attachment.to_s, 'image/png'),
    #     users: {user}\"]"
    # }
    send_post_with_file('/api/create_chat',attachment, user)
  end

  def search_user(username)
    body = {
        string: username
    }

    send_post('/api/user_search', body, 'body')['users']
  end

  def add_user(user_id)
    body={
        user_id: user_id
    }
    send_post('/api/contact_list/trusted_user/add', body, 'body')
  end

  def remove_user_from_contact_list(user_id)
    body = {
        user_id: user_id
    }
    send_post('/api/contact_list/trusted_user/remove', body, 'status')
  end

  def confirm_friendship_request(user)
    request = ERB::Util.url_encode("{\"accepted\":[\"#{user}\"]}")
    body="ids=#{request}"
    send_post_request('/api/contact_list/friendship/confirm_requests', body, 'status')
  end

  def get_contact_list
    send_post('/api/contact_list/get', '', 'body')
  end

  def create_group(name)
    body = { name: name }
    send_post('api/group/create', body, 'body')
  end

  def delete_group(group_id)
    body = { group_id: group_id }
    send_post('api/group/delete', body, 'body')
  end

  def add_user_to_group(group_id, user)
    if user.length > 1
      body = "group_id=#{group_id}&#{ERB::Util.url_encode("ids[]")}=#{user[0]}&#{ERB::Util.url_encode("ids[]")}=#{user[1]}"
    else
      body = "group_id=#{group_id}&#{ERB::Util.url_encode("ids[]")}=#{user[0]}"
    end
    send_post('api/group/add', body, 'body')
  end

  def get_groups_info
    send_post('/api/group', '','body')
  end

  def remove_user_from_group(group_id, user)
    if user.length > 1
      body = "group_id=#{group_id}&#{ERB::Util.url_encode("ids[]")}=#{user[0]}&#{ERB::Util.url_encode("ids[]")}=#{user[1]}"
    else
      body = "group_id=#{group_id}&#{ERB::Util.url_encode("ids[]")}=#{user[0]}"
    end
    send_post('api/group/remove', body, 'body')
  end

  def library_get_file(file_id)
    body = {
      file_id: file_id
    }
    send_post('/api/library/get_file', body, 'body')
  end

  def library_get_folder(folder_id)
    body = {
        folder_id: folder_id
    }
    send_post('/api/library/get_folder', body, 'body')
  end

  def set_group_avatar(group_id, file_meta)

    file_mime = file_meta['mime']
    filename = "attachments/1.png"
    file_as_string = File.open(filename, 'rb') { |f| f.read }
    encoded_file = Base64.encode64(file_as_string)
    file64 = "file64=data#{ERB::Util.url_encode(":#{file_mime};base64,#{encoded_file}")}&group_id=#{group_id}"

    body = file64

    send_post('/api/group/edit', body, 'body')
  end

  def get_document(parcel_id,comment_id)
    body={
        parcel_id: parcel_id,
        comment_id: comment_id
    }
  send_post('/api/conversation/get_document', body, 'body')
  end

  def get_notice
    send_post('/api/notice/get','','body')
  end

  def pull_back(parcel_id)
    body = {
        parcel_id:parcel_id
    }
    send_post('/api/newsfeed/pull_back', body, 'body')
  end

  def get_newsfeed
    body = {
        version: 4
    }
    send_post('/api/newsfeed/get', body, 'body')
  end

  def get_comment(conversation_id, comment_id)
    body = {
        conversation_id: conversation_id,
        comment_id: comment_id
    }
    send_post('/api/conversation/get_comment', body, 'body')
  end

end