
module CommonHelper
  require 'rubygems'

  def unique_value(string)
    if string.nil?
      ''
    elsif string.include? 'pre-setup'
      string.gsub('pre-setup ','')
    elsif string == ''
      ''
    else
      string+@random_string
    end
  end

  def unique_email_2(string)
    if string == ''
      return ''
    elsif string.include? 'pre-setup'
      return string.gsub('pre-setup ','')
    end
    email = string.split('@')
    "#{email.first}+#{@random_string_2}@#{email.last}"
  end

  def unique_email(string)
    if string == ''
      return ''
    elsif string.include? 'pre-setup'
      return string.gsub('pre-setup ','')
    end
    email = string.split('@')
    "#{email.first}+#{@random_string}@#{email.last}"
  end

  def parse_link_from(message)
    doc = Nokogiri::HTML(message)
    node = doc.css('a')
    node.to_s.split('"')[3] unless node.nil?
  end

  def delete_emails
    Gmail.new(EnvConfig.get(:api_email), EnvConfig.get(:api_password)) do |gmail|
      gmail.inbox.emails(:all, :from => EnvConfig.get(:notification_email)).each do |email|
        email.delete!
      end
    end
  end
end