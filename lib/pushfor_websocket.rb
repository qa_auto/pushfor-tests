require 'eventmachine'
require './features/support/env'
require 'wamp_client'



class PushforWebsocket

  def parse_event(chat=false)
    case chat
      when true
        File.foreach('console.txt') do |line|
          if line.include? 'chat_view_first'
            @event = line
          end
        end
      when false
              File.foreach('console.txt') do |line|
                if line.include? 'EVENT'
                  @event = line
                end
              end
    end

    return eval(eval(@event.split('> ').last).last.last.split('|||').last)
  end

  def get_message_socket(api,file, user)
    @stdout = $stdout
    $stdout = File.new('console.txt', 'w')
    $stdout.sync = true

    options = {
        uri: 'wss://uat.pushfor.com:65085/',
        realm: 'pushfor',
        verbose: true
    }
    connection = WampClient::Connection.new(options)

    connection.on_join do |session, details|

      # Register for something
      def add(args, kwargs, details)
        args[0] + args[1]
      end
      session.register(user, method(:add)) do |registration, error, details|
        handler = lambda do |args, kwargs, details|
          connection.close
        end
        session.subscribe(user, handler) do |subscription, error, details|
        end
        # Call It
        session.call(user, [3,4]) do |result, error, details|
          if result
            api.create_chat(file,user,'')
          end
        end
      end
    end
     connection.open
    $stdout = @stdout
    $stdout.sync = true
  end

  def get_friendship_socket(api,user)
    @stdout = $stdout
    $stdout = File.new('console.txt', 'w')
    $stdout.sync = true

    options = {
        uri: 'wss://uat.pushfor.com:65085/',
        realm: 'pushfor',
        verbose: true
    }
    connection = WampClient::Connection.new(options)

    connection.on_join do |session, details|

      # Register for something
      def add(args, kwargs, details)
        args[0] + args[1]
      end
      session.register(user, method(:add)) do |registration, error, details|
        handler = lambda do |args, kwargs, details|
          connection.close
        end
        session.subscribe(user, handler) do |subscription, error, details|
        end
        # Call It
        session.call(user, [3,4]) do |result, error, details|
          if result
            api.add_user(user)
          end
        end
      end
    end
    connection.open
    $stdout = @stdout
    $stdout.sync = true
  end


  def get_viewed_message_socket(api,parcel_id,comment_id,user)
    @stdout = $stdout
    $stdout = File.new('console.txt', 'w')
    $stdout.sync = true

    options = {
        uri: 'wss://uat.pushfor.com:65085/',
        realm: 'pushfor',
        verbose: true
    }
    connection = WampClient::Connection.new(options)

    connection.on_join do |session, details|

      # Register for something
      def add(args, kwargs, details)
        args[0] + args[1]
      end
      session.register(user, method(:add)) do |registration, error, details|
        handler = lambda do |args, kwargs, details|
          connection.close
        end
        session.subscribe(user, handler) do |subscription, error, details|
        end
        # Call It
        session.call(user, [3,4]) do |result, error, details|
          if result
            api.get_document(parcel_id,comment_id)
          end
        end
      end
    end
    connection.open
    $stdout = @stdout
    $stdout.sync = true
  end


  def get_message_socket_event(api, file, user)
    get_message_socket(api,file, user)

    parse_event
  end

  def get_friendship_socket_event(api, user)
    get_friendship_socket(api, user)

    parse_event
  end

  def get_viewed_message_socket_event(api,parcel_id,comment_id,user)
    get_viewed_message_socket(api,parcel_id,comment_id,user)
    parse_event(true)
  end
end
