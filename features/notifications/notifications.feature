Feature: Notifications

  Scenario: As an authorized user I should be able to receive a contact request
    #PFUAT-36
    When I register new user
    Then I should be able to receive a contact request

  Scenario: As an authorized user I should be able to receive a file
    #PFUAT-37
    When I register new user
    Then I should be able to receive a file

  Scenario: As an authorized user I should be able to receive a notification when the file I've pushed was viewed
    #PFUAT-38
    When I register new user
    Then I should be able to receive a notification about viewed file