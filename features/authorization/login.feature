Feature: Login

  Scenario: As a unauthorized user I should be able to register new user
    #PFUAT-7
    When I register new user

  Scenario: As a unauthorized user I should be able to authorize using Facebook oAuth
    #PFUAT-4
    Given I login via facebook
    Then Depending on the user, the service will response with different 200

#  Scenario: As a unauthorized user I should be able to authorize using Google+ oAuth
#  PFUAT-5
#    Given I login via google
#    Then Depending on the user, the service will response with different 200

  Scenario: As a unauthorized user I should be able to authorize using LinkedIn oAuth
    #PFUAT-6
    Given I login via linkedin
    Then Depending on the user, the service will response with different 200

  Scenario: User should be able to authorize via API with username and it should not be case-sensitive
    #PFUAT-11, PFUAT-10
    When I register new user
    Then I should be able to login with email and username

  Scenario: As a authorized user I should be able to logout
    #PFUAT-13
    When I register new user
    Then I should be able logout

  Scenario: As an unauthorized user I should not be able to create an account if the Email or Username is already in use
    #PFUAT-8 PFUAT-9
    When I register new user
    Then I should not be able to create user with same email or username

  Scenario: As an unauthorized user I should be able to change the Password of the account
    #PFUAT-12
    When I register new user
    Then I should be able to reset password

  Scenario: As an authorized user I should be able to change my email
    #PFUAT-39
    When I register new user
    Then I should be able to change my email