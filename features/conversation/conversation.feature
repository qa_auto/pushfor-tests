Feature: Conversation
	As an autorized user,
	I should be able to send and receive chat messages to
	My contacts who are in the conversation
	Who are added to my contact list

	# Scenario: The recipient should receive a message send to him in the conversation
	# 	Given I login as a "michael@pushfor.com" with password "Whiplash"
	# 	And I have a conversation with a user with ID "583ea7e8c1a23c7e148b456a"
	# 	When I send a chat message to the conversation
	# 	Then I login as a "gor" with password "111"
	# 	And I should receive a message from "michael@pushfor.com" as a "gor" with password "Whiplash"