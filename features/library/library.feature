Feature: Library

  Scenario: As an authorized user I should be able to upload a file to my Library
    #PFUAT-27
    When I register new user
    Then I upload file to library and get status

  Scenario: As an authorized user I should be able to search the Library for some particular file
    #PFUAT-28
    When I register new user
    And I upload file to library and get body
    Then I should be able search file

  Scenario: As an authorized user I should be able to delete multiple files at a time from my Library
    #PFUAT-34
    When I register new user
    And I upload file to library 5 times
    Then I delete all files

  Scenario: As an authorized user I should be able to delete file from my Library
    #PFUAT-32
    When I register new user
    And I upload file to library and get body
    Then I should be able delete file

  Scenario: As an authorized user I should be able to create a folder in my Library
    #PFUAT-35
    When I register new user
    Then I create folder in library

  Scenario: As an authorized user I should be able to download a file from my Library
    #PFUAT-33
    When I register new user
    Then I should be able to download file from library

  Scenario: As an authorized user I should be able to get file info of the particular file in Library
    #PFUAT-29
    When I register new user
    Then I should be able to get info about file from library

  Scenario: As an authorized user I should be able to push file from Library to a user
    #PFUAT-30
    When I register new user
    Then I should be able to push file from library

  Scenario: As an authorized user I should be able to push file from Library to a group
    #PFUAT-31
    When I register new user
    Then I should be able to push file from library to group