Feature: Push

    Scenario: As an authorized user I should be able to push a message to a user in my Contact List that will start a conversation
      #PFUAT-25
      When I register new user
      Then I should be able to push a message to a user in my contact list and start conversation

    Scenario: As an authorized user I should be able to push a message to a group of users
      #PFUAT-26
      When I register new user
      Then I should be able to push a message to group users in my contact list and start conversation

    Scenario: As an authorized user I should be able to pull back the content I've pushed to a user
      #PFUAT-21
      When I register new user
      Then I should be able to pull back the content

    Scenario: As an authorized user I should be able to upload and push a file to a user in my Contact List
      #PFUAT-19
      When I register new user
      Then I should be able to upload and push a file to a user in my Contact List
