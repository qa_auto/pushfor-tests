Feature: Groups

  Scenario: As an authorized user I should be able to create a group
    #PFUAT-41
    When I register new user
    Then I should be able to create new group

  Scenario: As an authorized user I should be able to delete my group
    #PFUAT-42
    When I register new user
    Then I should be able to delete my group

  Scenario: As an authorized user I should be able to add a user to my group
    #PFUAT-43
    When I register new user
    Then I should be able to add user to my group

  Scenario: As an authorized user I should be able to remove a user from my group
    #PFUAT-44
    When I register new user
    Then I should be able to remove user from my group

  Scenario: As an authorized user I should be able to change the group avatar
    #PFUAT-45
    When I register new user
    Then I change the group avatar using the picture from my Library
