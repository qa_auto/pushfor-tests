Feature: Contact List

  Scenario: As an authorized user I should be able to search for users not in my Contact List by their username
    #PFUAT-40
    When I register new user
    Then I should be able to search another user by username which is not in my contact list

  Scenario: As an authorized user I should be able to send contact request to another user.
    #PFUAT-14
    When I register new user
    Then The recipient should be added to my Contact List once the request is accepted

  Scenario: As an authorized user I should be able to remove users from my Contact List
    #PFUAT-15
    When I register new user
    Then I should be able to remove users from my contact list
