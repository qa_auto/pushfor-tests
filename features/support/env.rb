$LOAD_PATH << File.dirname(__FILE__) + '/../lib'
ENVIRONMENT = (ENV['ENVIRONMENT'] || 'production').to_sym
raise "You need to create a configuration file named '#{ENVIRONMENT}.yml' under lib/config" unless File.exist? "#{File.dirname(__FILE__)}/../../lib/config/#{ENVIRONMENT}.yml"

require 'capybara/cucumber'
require 'httparty'
require 'pushfor_api'
require 'env_config'
require 'faker'
require 'common_helper'
require 'websocket'
require 'eventmachine'
require 'timestamped/html_formatter'

World(CommonHelper)

def before_scenario(scenario)
@random_string = Faker::Lorem.characters(4)
@random_string_2 = Faker::Lorem.characters(5)
end

Before do |scenario|
  before_scenario(scenario)
end

# Parse the 'set-cookie' string
# @param [String] all_cookies_string
# @return [Hash]
# def parse_set_cookie(all_cookies_string)
#   cookies = Hash.new
#
#   if all_cookies_string
#     # single cookies are devided with comma
#     all_cookies_string.split(',').each {
#       # @type [String] cookie_string
#         |single_cookie_string|
#       # parts of single cookie are seperated by semicolon; first part is key and value of this cookie
#       # @type [String]
#       cookie_part_string  = single_cookie_string.strip.split(';')[0]
#       # remove whitespaces at beginning and end in place and split at '='
#       # @type [Array]
#       cookie_part         = cookie_part_string.strip.split('=')
#       # @type [String]
#       key                 = cookie_part[0]
#       # @type [String]
#       value               = cookie_part[1]
#
#       # add cookie to Hash
#       cookies[key] = value
#     }
#   end
#
#   cookies
# end

# Capybara.default_driver = :selenium


Capybara.default_driver = :poltergeist
