When(/^Second user should receive file from me$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  file_info = api.get_file_info(@response['id'])
  first_user_info = api.get_user_info
  second_user_info = api_2.get_user_info
  websocket = PushforWebsocket.new
  event = websocket.get_message_socket_event(api,file_info['id'],second_user_info['info']['id'])

  expect(event[:text_ios]).to eq("File from #{first_user_info['info']['username']}")
  expect(event[:text]).to eq("File from #{first_user_info['info']['username']}")
  expect(event[:type]).to eq('comment_added')
  expect(event[:sender_id]).to eq(first_user_info['info']['id'])
end

When(/^I check notification about viewed file$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  file_info = api.get_file_info(@response['id'])
  first_user_info = api.get_user_info
  second_user_info = api_2.get_user_info
  response = api.create_chat(file_info['id'],second_user_info['info']['id'],[])

  websocket = PushforWebsocket.new
  event = websocket.get_viewed_message_socket_event(api_2,response['parcels_sent'][0]['parcel_id'],response['comment_data'][0]['comment_ids'][0], first_user_info['info']['id'])

  expect(event[:text_ios]).to eq("#{second_user_info['info']['username']} viewed #{file_info['name']}")
  expect(event[:text]).to eq("viewed #{file_info['name']}")
  expect(event[:type]).to eq('chat_view_first')
  expect(event[:sender_id]).to eq(second_user_info['info']['id'])
  expect(event[:recipient_id]).to eq(first_user_info['info']['id'])

end


Then(/^I should be able to receive a contact request$/) do
  step "I register second user"
  step "I add second user to my contact list and receive notification"
end

Then(/^I should be able to receive a file$/) do
  step "I register second user"
  step "I upload file to library and get body"
  step "I add second user to my contact list"
  step "Second user should receive file from me"
end

Then(/^I should be able to receive a notification about viewed file$/) do
  step "I register second user"
  step "I upload file to library and get body"
  step "I add second user to my contact list"
  step "I check notification about viewed file"
end
