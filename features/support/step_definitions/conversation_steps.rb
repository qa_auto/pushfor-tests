API_ADD_COMMENT = "/api/conversation/add_comment"
API_GET_COMMENT = "/api/conversation/get_comment"


ADD_COMMENT_TEXT = "😂😂😂 This is a test message, it should be sent and received succesfully. 
Some whitespaces.
Some garbage: 123456789, .|\\ // 
Some emojiis: 😂😂😂"

Given(/^I have a conversation with a user with ID "([^"]*)"$/) do |id|
  formBody = { comment: CREATE_CHAT_TEXT, users: '["'+id+'"]'}

  headers = { 
	  "Cookie" => "REMEMBERME="+ @userA_set_cookie_hash['REMEMBERME'] + ";" 
	}

  @create_chat_response = HTTParty.post(URL + API_CREATE_CHAT, :body=> formBody, :headers => headers)
  expect(@create_chat_response.code).to eq(200)
  @conversation_id = @create_chat_response["comment_data"][0]['conversation_id']
end

When(/^I send a chat message to the conversation$/) do
  formBody = { conversation_id: @conversation_id, 
  			   comment: ADD_COMMENT_TEXT
  			}

  headers = { 
	  "Cookie" => "REMEMBERME="+ @userA_set_cookie_hash['REMEMBERME'] + ";" 
	}

  @add_comment_response = HTTParty.post(URL + API_ADD_COMMENT, :body=> formBody, :headers => headers)
  expect(@add_comment_response.code).to eq(200)
  @comment_id =  @add_comment_response["comment_ids"][1] #because 0 is null as a topic starter
end

Then(/^I should receive a message from "([^"]*)" as a "([^"]*)" with password "([^"]*)"$/) do |arg1, arg2, arg3|
  formBody = { conversation_id: @conversation_id, 
  			   comment_id: @comment_id,
  			   version: 4
  			}

  headers = { 
	  "Cookie" => "REMEMBERME="+ @userA_set_cookie_hash['REMEMBERME'] + ";" 
	}

  @get_comment_response = HTTParty.post(URL + API_GET_COMMENT, :body=> formBody, :headers => headers)
  expect(@add_comment_response.code).to eq(200)
  expect(@get_comment_response['text']).to eq(ADD_COMMENT_TEXT)
end



