require './features/support/env'
require 'gmail'

# When(/^I open link from email with subject '(.*)' addressed to '(.*)'$/) do |subject, email_address|
#   i = 0
#   @email_link = nil
#   while (@email_link == nil) && (i < 30)
#     Gmail.new(EnvConfig.get(:api_email), EnvConfig.get(:api_password)) do |gmail|
#       email = gmail.inbox.emails(:unread,
#                                  :from => EnvConfig.get(:notification_email),
#                                  subject: subject,
#                                  :to => unique_email(email_address)).last
#       if email.nil? == false
#         @email_link = parse_link_from(email.body.decoded)
#       end
#     end
#     sleep 2
#     i += 1
#   end
#   if (@email_link == nil)
#     raise ArgumentError.new "Invitation link was not found. Probably user didn't received invitation email or link is broken."
#   end
#   puts "\nChecked mail for '#{unique_email(email_address)}' email"
#   puts "\nOpened link: " + @email_link.to_s
#   delete_emails
#
#   api = PushforApi.new('','',false)
#   api.send_get(@email_link)
#   @link_change_pass = @email_link.split('/').last
#
# end