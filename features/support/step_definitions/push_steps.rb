require 'json'

When(/^I push file to second user$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  second_user_info = api_2.get_user_info
  api.create_chat_with_file(second_user_info['info']['id'])
end

When(/^I push message to user in my contact list$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  second_user_info = api_2.get_user_info
  response = api.create_chat('',second_user_info['info']['id'],'')
  expect(response['comment_data'][0].key?('conversation_id'))
  expect(!response['comment_data'][0]['conversation_id'].nil?)
end

When(/^I push message to group in my contact list$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  group_info = api.get_groups_info
  response = api.create_chat('','',group_info[0]['id'])
  expect(response['comment_data'][0].key?('conversation_id'))
  expect(!response['comment_data'][0]['conversation_id'].nil?)
end

When(/^I pull back the content$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  message_list_1 = api.get_newsfeed
  message_list_2 = api_2.get_newsfeed
  expect(message_list_1[0]['active']).to eq(true)
  expect(message_list_2.empty?).to eq(false)
  response = api.pull_back(message_list_1[0]['id'])
  message_list_1 = api.get_newsfeed
  message_list_2 = api_2.get_newsfeed
  expect(message_list_1[0]['active']).to eq(false)
  expect(message_list_2.empty?).to eq(true)
end

And(/^I push new file to another user as a new message$/) do
  api1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)

  user2_info = api2.get_user_info

  current_chat = api1.create_chat_with_attach(user2_info['info']['id'])


  @conversation_id = current_chat['comment_data'][0]['conversation_id']
  @comment_id = current_chat['comment_data'][0]['comment_ids'][0]
  @user1_file_id = current_chat['comment_data'][0]['file_id']
end

And(/^Another user receives transmitted file$/) do
  api2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  @user2_file_id = api2.get_comment(@conversation_id, @comment_id)['file']['id']
  expect(@user1_file_id == @user2_file_id).to be true
end


#===============================================================================

Then(/^I should be able to upload and push file to user in my contact list$/) do
  step "I register second user"
  step "I push file to second user"
end

Then(/^I should be able to push a message to a user in my contact list and start conversation$/) do
  step "I register second user"
  step "I add second user to my contact list"
  step "I push message to user in my contact list"
end

Then(/^I should be able to push a message to group users in my contact list and start conversation$/) do
  step "I register second user"
  step "I register third user"
  step "I add second user to my contact list"
  step "I add third user to my contact list"
  step "I create group with 3 users"
  step "I push message to group in my contact list"
end

Then(/^I should be able to upload and push a file to a user in my Contact List$/) do
  step "I register second user"
  step "I add second user to my contact list"
  step "I push new file to another user as a new message"
  step "Another user receives transmitted file"
end

Then(/^I should be able to pull back the content$/) do
  step "I register second user"
  step "I add second user to my contact list"
  step "I push message to user in my contact list"
  step "I pull back the content"
end


