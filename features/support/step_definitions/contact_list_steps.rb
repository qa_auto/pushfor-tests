require './features/support/env'

When(/^I search another user in application by username and find him$/) do
  api_1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  second_user_name = api_2.get_user_info['info']['username']
  users_list = api_1.search_user(second_user_name)
  wanted_user = users_list[0]['username']
  expect(wanted_user.to_s == second_user_name.to_s).to be true
end

When(/^I delete user from my contact list$/) do
  api_1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)

  second_user_id = api_2.get_user_info['info']['id']
  user_removal = api_1.remove_user_from_contact_list(second_user_id)
  expect(user_removal).to eq(200)

  first_user_list = api_1.get_contact_list
  second_user_list = api_2.get_contact_list
  expect(first_user_list['trusted_users'].empty?).to eq(true)
  expect(second_user_list['trusted_users'].empty?).to eq(true)

end

Then(/^I ensure that user is not in my contact list$/) do
  api_1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  first_user_contacts = api_1.get_contact_list['trusted_users']
  second_user_name = api_2.get_user_info['info']['username']
  is_contact_in_contact_list = false
  first_user_contacts.each do |user|
    if user['username'].to_s == second_user_name.to_s
      is_contact_in_contact_list = true
    end
  end
  expect(is_contact_in_contact_list).to be false
end

#===================================================================

Then(/^I should be able to search another user by username which is not in my contact list$/) do
  step "I register second user"
  step "I search another user in application by username and find him"
  step "I ensure that user is not in my contact list"
end

Then(/^The recipient should be added to my Contact List once the request is accepted$/) do
  step "I register second user"
  step "I add second user to my contact list"
end


Then(/^I should be able to remove users from my contact list$/) do
  step "I register second user"
  step "I add second user to my contact list"
  step "I delete user from my contact list"
end

