When(/^I delete all my groups$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api.get_groups_info.each do |group|
    api.delete_group(group['id'])
  end
end

Then(/^I delete existing user from my group$/) do
  api_1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  second_user_info = api_2.get_user_info
  wanted_group_id = nil
  api_1.get_groups_info.each do |group|
    wanted_group_id = group['id'] if group['name'].eql?('test')
  end
  api_1.remove_user_from_group(wanted_group_id, second_user_info['info']['id'])
end

When(/^I set new group's avatar and ensure about that$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  wanted_group_id = nil
  api.get_groups_info.each do |group|
    wanted_group_id = group['id'] if group['name'].eql?('test')
  end
  uploaded_file_id = api.upload_file('body')['id']
  file_meta = api.library_get_file(uploaded_file_id)
  response = api.set_group_avatar(wanted_group_id, file_meta)
  expect(response['avatar']['success']).to be true
end

Then(/^I ensure that my user already owns groups$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  expect(api.get_groups_info.empty?).to be false
end

Then(/^I ensure that my user hasn't any groups$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  expect(api.get_groups_info.empty?).to be true
end

Then(/^I ensure that another user included into group$/) do
  api_1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  second_user_info = api_2.get_user_info
  is_user_in_the_group = false
  users_list = nil
  api_1.get_groups_info.each do |group|
    users_list = group['users'] if group['name'].eql?('test')
  end
  users_list.each do |user|
    is_user_in_the_group = true if user['id'].eql?(second_user_info['info']['id'])
  end
  expect(is_user_in_the_group).to be true
end

Then(/^I ensure that deleted user already removed from my group$/) do
  api_1 = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  second_user_id = api_2.get_user_info['info']['id']
  all_users_groups = api_1.get_groups_info
  users_list = nil
  all_users_groups.each do |group|
    users_list = group['users'] if group['name'].eql?('test')
  end
  expect(users_list).not_to include(second_user_id)
end


#=================================
Then(/^I should be able to create new group$/) do
  step "I register second user"
  step "I add second user to my contact list"
  step "I create group with 2 users"
  step "I ensure that my user already owns groups"
end

Then(/^I should be able to delete my group$/) do
  step "I should be able to create new group"
  step "I delete all my groups"
  step "I ensure that my user hasn't any groups"
end

Then(/^I should be able to add user to my group$/) do
  step "I should be able to create new group"
  step "I ensure that another user included into group"
end

Then(/^I should be able to remove user from my group$/) do
   step "I should be able to add user to my group"
   step "I delete existing user from my group"
   step "I ensure that deleted user already removed from my group"
end

Then(/^I change the group avatar using the picture from my Library$/) do
  step "I should be able to create new group"
  step "I set new group's avatar and ensure about that"
end


