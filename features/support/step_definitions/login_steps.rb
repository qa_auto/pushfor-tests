require './features/support/env'

When(/^I open link from email with subject '(.*)' addressed to (.*) (.*)$/) do |subject, version, email_address|
  i = 0
  if version == 'first'
    to = unique_email(email_address)
  else
    to = unique_email_2(email_address)
  end
  @email_link = nil
  while (@email_link == nil) && (i < 30)
    Gmail.new(EnvConfig.get(:api_email), EnvConfig.get(:api_password)) do |gmail|
      email = gmail.inbox.emails(:unread,
                                 :from => EnvConfig.get(:notification_email),
                                 :subject => subject,
                                 :to =>to
      ).last
      if email.nil? == false
        @email_link = parse_link_from(email.body.decoded)
      end
    end
    sleep 2
    i += 1
  end
  if (@email_link == nil)
    raise ArgumentError.new "Invitation link was not found. Probably user didn't received invitation email or link is broken."
  end
  # puts "\nChecked mail for '#{to}' email"
  # puts "\nOpened link: " + @email_link
  delete_emails

  @api = PushforApi.new('','',false)
  @api.send_get(@email_link.gsub('http://','https://'))
  @api.confirm_reg(@email_link.split('=')[1])

  @link_change_pass = @email_link.split('/').last
end

When(/^I login via (.*)$/) do |social|
  api = PushforApi.new('','',false)
  case social
    when 'facebook'
      @response = api.login_via_facebook
    when 'linkedin'
      @response = api.login_via_linkedin
    when 'google'
      @response = api.login_via_google
  end
end

Given(/^The User ([^"]*) login with the password ([^"]*)$/) do |user, pass|
  if user.include? '@'
    usename = unique_email(user)
  else
    usename = unique_value(user)
  end
  @api = PushforApi.new('','',false)
  @response = @api.login(usename,pass)
end

When(/^user logout$/) do
  @response = @api.logout
end

When(/^I create a new user with following details:$/) do |table|
  @api = PushforApi.new('','',false)
  table.hashes.each do |user|
    username = user[:username]
    email = user[:email]
    password = user[:password]
    status = user[:status]
  @response = @api.register(unique_value(username),unique_email(email),password,status)
  end
end

And(/^Send confirmation email to (.*)$/) do |email|
  @api = PushforApi.new('','',false)
  response = @api.send_register_confirmation(unique_email(email))
end

When(/^User change password to (.*)$/) do |new_pass|
  @api = PushforApi.new('','',false)
  response = @api.change_password_to_new(new_pass, @link_change_pass)
  expect(response).to eq(200)
end

When(/^User send reset password mail to (.*)$/) do |email|
  api = PushforApi.new('','',false)
  api.send_reset_password_link(unique_email(email))
end

When(/User (.*) change account details:/) do |users, table|
  if users.include? '@'
    usename = unique_email(users)
  else
    usename = unique_value(users)
  end
  api = PushforApi.new(usename,'Great123',true)
  table.hashes.each do |user|
    username = user[:username]
    email = user[:email]
    password = user[:password]
    response = api.change_user_info(unique_value(username),unique_email_2(email), password)
    expect(response['email']['success']).to eq(true)
  end
end

When(/User (.*) get own account info/) do |user|
  api = PushforApi.new(unique_value('pushforqa'),'Great123',true)
  response = api.get_user_info
  expect(response['info']['email']).to eq(unique_email_2(user))
end


Then(/^Depending on the user, the service will response with different ([^"]*)$/) do |status|
  expect(@response).to  eq(status.to_i)
end

Then(/^Depending on the user, the service will response with ([^"]*) body$/) do |status|
  expect(@response['message']).to  eq(status)
end

Then(/^the response status should be (\d+)$/) do |status|
  expect(@response).to  eq(status.to_i)
end


#=================================================================
When(/^I register (.*) user$/) do |number|
case number
  when 'new'
    username = 'pushforqa'
    email = 'pushforqa@gmail.com'
  when 'second'
    username = 'pushforqa2'
    email = 'pushforqa+2@gmail.com'
  when 'third'
    username = 'pushforqa3'
    email = 'pushforqa+3@gmail.com'
end
  password = 'Great123'
  status = 'status'

  step "I create a new user with following details:",
       table(
           %{| username    | email    | password    | status    |
             | #{username} | #{email} | #{password} | #{status} |
            })
  step "Depending on the user, the service will response with different 200"
  step "I open link from email with subject 'Pushfor Account Activation' addressed to first #{email}"
  step "user logout"
  step "The User #{username} login with the password #{password}"
  step "Depending on the user, the service will response with different 200"
  step "user logout"
  # step "The User '#{email}' login with the password '#{password}'"
  # step "Depending on the user, the service will response with different '200'"
  # step "user logout"
end

When(/^(.*) user (.*) with (.*) pass$/) do |user, action, pass|
  step "The User #{user} login with the password #{pass}"
  case action
    when 'login'
      step "Depending on the user, the service will response with different 200"
    when 'not login'
      step "Depending on the user, the service will response with different 400"
  end
end

When(/^user login or not login with pass$/) do |table|
  table.hashes.each do |user|
    username = user[:user]
    pass = user[:password]
    action = user[:status]

  step "The User #{username} login with the password #{pass}"
  case action
    when 'login'
      step "Depending on the user, the service will response with different 200"
    when 'not login'
      step "Depending on the user, the service will response with different 400"
  end
  end
end

When(/^User (.*) reset password to (.*)$/) do |user,pass|
  step "User send reset password mail to #{user}@gmail.com"
  step "I open link from email with subject 'Pushfor Reset Password Confirmation' addressed to first #{user}@gmail.com"
  step "User change password to #{pass}"
end

When(/^User change own email$/) do
  step "User pushforqa change account details:",
  table(
      %{|email              | username   |
        |pushforqa@gmail.com| pushforqa |
            })
  step "I open link from email with subject 'Pushfor Account Changing' addressed to second pushforqa@gmail.com"
  step "user logout"
end

When(/^User change own email to default$/) do
  step "User pre-setup pushfor qa change account details:",
       table(
           %{|email                        | username   |
             |pre-setup pushforqa@gmail.com| pushfor qa |
            })
  step "I open link from email with subject 'Pushfor Account Changing' addressed to pre-setup pushforqa@gmail.com"
  step "user logout"
end

When(/^User check that account changed$/) do
  step "User pushforqa@gmail.com get own account info"
end


When(/^I should be able to login with email and username$/) do
  step "user login or not login with pass",
    table(
        %{
      | user                | password | status    |
      | pushforqa           | Great123 | login     |
      | pushforqa           | 111      | not login |
      | PUSHFORQA           | Great123 | login     |
      | PUSHFORQA           | 111      | not login |
      # | pushforqa@gmail.com | Great123 | login     |
      # | pushforqa@gmail.com | 111      | not login |
      # | PUSHFORQA@gmail.com | Great123 | login     |
      # | PUSHFORQA@gmail.com | 111      | not login |
})
end

Then(/^I should be able to reset password$/) do
  step "pushforqa user login with Great123 pass"
  step "User pushforqa reset password to Great321"
  step "pushforqa user login with Great321 pass"
  step "pushforqa user not login with Great123 pass"
end

Then(/^I should be able to change my email$/) do
  step "pushforqa user login with Great123 pass"
  step "User change own email"
  step "User check that account changed"
#      step "'pre-setup pushforqa@gmail.com' user 'not login' with 'Great123' pass"
#      step "'pushforqa@gmail.com' user 'login' with 'Great123' pass"
end

Then(/^I should not be able to create user with same email or username$/) do
  step "I create a new user with following details:", table(%{
      | username  | email                | password | status |
      | pushforqa | pushforqat@gmail.com | Great123 | body   |
  })
  step "Depending on the user, the service will response with This username was already registered body"
  step "I create a new user with following details:", table(%{
      | username   | email               | password | status |
      | pushforqat | pushforqa@gmail.com | Great123 | body   |
  })
  step "Depending on the user, the service will response with This email was already registered body"
end

Then(/^I should be able logout$/) do
  step "pushforqa user login with Great123 pass"
  step "user logout"
  step "the response status should be 200"
end



