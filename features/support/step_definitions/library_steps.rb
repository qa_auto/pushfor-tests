When(/^I upload file to library and get (.*)$/) do |status|
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.upload_file(status)
  case status
    when 'status'
      expect(response).to eql(200)
    when 'body'
      @response = response
  end
end

When(/^I delete all files$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.delete_all_files
end

When(/^I create folder in library$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.create_folder(unique_value('test'))
  expect(response).to eq(200)
end

When(/^I download file from library$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.get_file_info(@response['id'])
  get_link = api.download_file(response['hash'])
  expect(get_link).to eq(200)
end

When(/^I get file info$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.get_file_info(@response['id'])
  expect(response['id']).to eq(@response['id'])
  expect(response['owner']).to eq(unique_value('pushforqa'))
  expect(response['owner_username']).to eq(unique_value('pushforqa'))
end

When(/^I push file from library to second user$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  file_info = api.get_file_info(@response['id'])
  second_user_info = api_2.get_user_info
  response = api.create_chat(file_info['id'],second_user_info['info']['id'],[])
  expect(!response['parcels_sent'].empty?).to eq(true)
  expect(!response['comment_data'].empty?).to eq(true)
end

When(/^I push file to group$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  file_info = api.get_file_info(@response['id'])
  group_info = api.get_groups_info
  response = api.create_chat(file_info['id'],[],group_info[0]['id'])
  expect(!response['parcels_sent'].empty?).to eq(true)
  expect(!response['comment_data'].empty?).to eq(true)
end

When(/^I add (.*) user to my contact list$/) do |number_of_user|
  case number_of_user
    when 'second'
      api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
      api_user = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
      first_user_info = api.get_user_info
      second_user_info = api_user.get_user_info
      add_user = api.add_user(second_user_info['info']['id'])
      expect(add_user['success']).to eq(true)

      first_user_list = api.get_contact_list
      second_user_list = api_user.get_contact_list
      expect(first_user_list['trusted_users'].empty?).to eq(true)
      expect(second_user_list['trusted_users'].empty?).to eq(true)

      confirmation = api_user.confirm_friendship_request(first_user_info['info']['id'])
      expect(confirmation).to eq(200)

      first_user_list = api.get_contact_list
      second_user_list = api_user.get_contact_list
      expect(first_user_list['trusted_users'][0]['id']).to eq(second_user_info['info']['id'])
      expect(second_user_list['trusted_users'][0]['id']).to eq(first_user_info['info']['id'])
      expect(first_user_list['trusted_users'][0]['username']).to eq(second_user_info['info']['username'])
      expect(second_user_list['trusted_users'][0]['username']).to eq(first_user_info['info']['username'])
    when 'third'
      api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
      api_user = PushforApi.new(unique_value('pushforqa3'), 'Great123', true)
      first_user_info = api.get_user_info
      third_user_info = api_user.get_user_info
      add_user = api.add_user(third_user_info['info']['id'])
      expect(add_user['success']).to eq(true)

      first_user_list = api.get_contact_list
      third_user_list = api_user.get_contact_list
      expect(first_user_list['trusted_users'][1].nil?).to eq(true)
      expect(third_user_list['trusted_users'].empty?).to eq(true)

      confirmation = api_user.confirm_friendship_request(first_user_info['info']['id'])
      expect(confirmation).to eq(200)

      first_user_list = api.get_contact_list
      third_user_list = api_user.get_contact_list
      expect(first_user_list['trusted_users'][1]['id']).to eq(third_user_info['info']['id'])
      expect(third_user_list['trusted_users'][0]['id']).to eq(first_user_info['info']['id'])
      expect(first_user_list['trusted_users'][1]['username']).to eq(third_user_info['info']['username'])
      expect(third_user_list['trusted_users'][0]['username']).to eq(first_user_info['info']['username'])
  end
end

When(/^I add second user to my contact list and receive notification$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  api_user = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
  first_user_info = api.get_user_info
  second_user_info = api_user.get_user_info

  websocket = PushforWebsocket.new
  event = websocket.get_friendship_socket_event(api,second_user_info['info']['id'])

  expect(event[:text_ios]). to eq("#{first_user_info['info']['username']} requested to add you to Contact List")
  expect(event[:text]). to eq("#{first_user_info['info']['username']} requested to add you to Contact List")
  expect(event[:type]).to eq('friendship_request')
  expect(event[:sender_id]).to eq(first_user_info['info']['id'])
end

When(/^I create group with (.*) users$/) do |users_count|
  case users_count
    when '2'
      api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
      api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
      second_user_info = api_2.get_user_info
      response = api.create_group('test')
      api.add_user_to_group(response['group_id'], [second_user_info['info']['id']])
    when '3'
      api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
      api_2 = PushforApi.new(unique_value('pushforqa2'), 'Great123', true)
      api_3 = PushforApi.new(unique_value('pushforqa3'), 'Great123', true)
      second_user_info = api_2.get_user_info
      third_user_info = api_3.get_user_info
      response = api.create_group('test')
      users = ["#{second_user_info['info']['id']}","#{third_user_info['info']['id']}"]
      api.add_user_to_group(response['group_id'], users)
  end

end


Then(/^I should be able search file$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.search_file(@response['name'], @response['folder_id'], 'body')
  expect(response[0]['name']).to eql(@response['name'])
  expect(response[0]['parent']).to eql(@response['folder_id'])
end

Then(/^I should be able delete file$/) do
  api = PushforApi.new(unique_value('pushforqa'), 'Great123', true)
  response = api.delete_file(@response['id'], 'body')
  expect(response['id']).to eql(@response['id'])
end


#=========================================================
When(/^I upload file to library (.*) times$/) do |times|
  for i in 1..times.to_i
  step "I upload file to library and get status"
  end
end

Then(/^I should be able to download file from library$/) do
  step "I upload file to library and get body"
  step "I download file from library"
end

Then(/^I should be able to get info about file from library$/) do
  step "I upload file to library and get body"
  step "I get file info"
end

Then(/^I should be able to push file from library$/) do
  step "I register second user"
  step "I upload file to library and get body"
  step "I add second user to my contact list"
  step "I push file from library to second user"
end

Then(/^I should be able to push file from library to group$/) do
  step "I register second user"
  step "I upload file to library and get body"
  step "I add second user to my contact list"
  step "I create group with 2 users"
  step "I push file to group"
  end
