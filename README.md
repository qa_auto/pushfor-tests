# Pushfor Acceptance Tests #

This is a repository with Pushfor Acceptance tests. It's completely independent of Pushfor architecture and can be run anywhere. It should be run every time a new release is pushed to Test environment.


### How do I get set up? (MacOS) ###

* Install Brew

```
#!shell

http://brew.sh
```

* Install Ruby

```
#!shell

brew install ruby
```

* Install Bundler

```
#!shell

gem install bundler
```

* Install Firefox


```
#!shell

https://www.mozilla.org/en-US/firefox/new/
```

* Run


```
#!shell

bundle
cucumber
```